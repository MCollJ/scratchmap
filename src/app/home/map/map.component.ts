import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  countries = [];

  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {
    $(document).ready(() =>
      $('#world-map').vectorMap({
        map: 'world_en',
        backgroundColor: 'none',
        enableZoom: false,
        showTooltip: true,
        color: '#364B53',
        borderColor: '#445860',
        borderOpacity: 0,
        hoverColor: '#f5c026',
        selectedColor: '#f5c026',
        multiSelectRegion: true
      })
    );
    // this.sizeMap();
    // $(window).on("resize", this.sizeMap);
    // $("#world-map").vectorMap({
    //   map: "world_en",
    //   backgroundColor: "#a5bfdd",
    //   borderColor: "#818181",
    //   borderOpacity: 0.25,
    //   borderWidth: 1,
    //   color: "#f4f3f0",
    //   enableZoom: true,
    //   hoverColor: "#c9dfaf",
    //   hoverOpacity: null,
    //   normalizeFunction: "linear",
    //   scaleColors: ["#b6d6ff", "#005ace"],
    //   selectedColor: "#c9dfaf",
    //   selectedRegions: null,
    //   showTooltip: true,
    //   onRegionClick: function(element, code, region) {
    //     var message =
    //       'You clicked "' +
    //       region +
    //       '" which has the code: ' +
    //       code.toUpperCase();
    //     alert(message);
    //   }
    // });
  }
}
